//go:generate protoc -I/usr/local/include -I../walletpb -I$GOPATH/src  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --go_out=plugins=grpc:../walletpb ../walletpb/walletpb.proto

package issuer

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/objectcomputing/issuerservice/config"
	"bitbucket.org/objectcomputing/issuerservice/walletpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// Implements of IssuerServer
type issuerServer struct{}

// IssuerID represents the identifier of the issuer
var IssuerID string

// Customers data from config
var Customers = make(map[string][]*config.Wallet)

// NewIssuerServer creates a new issuer server
func NewIssuerServer() walletpb.IssuerServer {
	return new(issuerServer)
}

func (s *issuerServer) Get(ctx context.Context, msg *walletpb.WalletRequest) (*walletpb.WalletReply, error) {

	t := time.Now().UTC()

	fmt.Printf("%v: Issuer %v received msg: %v\n", t, IssuerID, msg)

	err := grpc.SendHeader(ctx, metadata.New(map[string]string{
		"transaction_id": msg.TransactionId,
		"uid":            strings.ToLower(msg.Uid),
	}))

	if err != nil {
		return nil, err
	}

	userWallets, ok := Customers[strings.ToLower(msg.Uid)]

	if ok {
		wallets := make([]*walletpb.Wallet, len(userWallets))
		for i, w := range userWallets {
			wallets[i] =
				&walletpb.Wallet{
					IssuerId:      IssuerID,
					LastName:      w.LastName,
					FirstName:     w.FirstName,
					StreetAddress: w.StreetAddress,
					City:          w.City,
					StateProvince: w.StateProvince,
					PostalCode:    w.PostalCode,
					CountryName:   w.CountryName,
					PaymentToken:  w.CardNumber,
					Icon:          w.Icon}
		}
		reply := walletpb.WalletReply{
			TransactionId: msg.TransactionId,
			Uid:           msg.Uid,
			Wallets:       wallets,
		}
		return &reply, nil
	}
	return nil, status.Errorf(codes.NotFound, "not found")
}
