# Issuer Service

Given a transaction ID and a user ID, the issuer service returns a set of wallet data associated with the user ID. The assocations are defined in the configuration file. It supports both GRPC and HTTP Get method.

### Configuration

Issuer Service supports reading config files in JSON, TOML, YAML, HCL, and Java properties formats. It should be located in the `/etc/monster_mesh/issuer_service` or current working directory with `config` as file base name. A sample config file `config.yml` is in the top level directory of the repository.

By default, it listens at the TCP ports 8080 and 9090 for HTTP and GRPC protocols if http_port and grpc_port are not specified in the config file.

###  A Test Run

```bash
docker build . -t issuer
docker run --rm -p 8080:8080 -p 9090:9090 --name issuer_ep -d issuer
curl -X GET -k http://localhost:8080/v1/wallets/transaction_id/abc/uid/johndoe@gmail.com
# clean up the resource
docker stop issuer_ep
```
