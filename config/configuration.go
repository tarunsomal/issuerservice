package config

type Wallet struct {
	LastName      string `mapstructure:"last_name"`
	FirstName     string `mapstructure:"first_name"`
	StreetAddress string `mapstructure:"street_address"`
	City          string `mapstructure:"city"`
	StateProvince string `mapstructure:"state_province"`
	PostalCode    string `mapstructure:"postal_code"`
	CountryName   string `mapstructure:"country_name"`
	CardNumber    string `mapstructure:"card_number"`
	Icon          string
}

type Customer struct {
	UID     string
	Wallets []*Wallet
}

// Configuration defines the structure of config file
type Configuration struct {
	HTTPPort  int    `mapstructure:"http_port"`
	GRPCPort  int    `mapstructure:"grpc_port"`
	IssuerID  string `mapstructure:"issuer_id"`
	Customers []*Customer
}
