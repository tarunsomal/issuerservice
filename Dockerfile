FROM golang:1.9.2

WORKDIR /go/src/bitbucket.org/objectcomputing/issuerservice/
COPY . .

RUN go-wrapper download   # "go get -d -v ./..."
RUN go-wrapper install    # "go install -v ./..."

# HTTP PORT
EXPOSE 8080

# GRPC PORT
EXPOSE 9090

CMD ["go-wrapper", "run"] # ["app"]
